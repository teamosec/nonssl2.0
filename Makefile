GO_EASY_ON_ME = 1

include theos/makefiles/common.mk

TWEAK_NAME = nonssl20
nonssl20_FILES = nonssl20.mm
nonssl20_FRAMEWORKS =

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "mv -f /Library/MobileSubstrate/DynamicLibraries/nonssl20.dylib /usr/lib &&  killall -9 backboardd"
