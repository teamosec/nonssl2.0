# nonSSL2.0 #

 Nonssl is an injected dynamic library that cancels ssl certificate validation by accepting any certificate (see nonssl documentation https://bitbucket.org/teamosec/nonssl).
 
 Version 2.0 of nonssl implements its own injection, function hooking and loading method without jaillbreak dependencies.

## What it does? ##

### injection: ###

 For injection it uses the DYLID_INSERT_LIBRARIES environment variable, a colon separated list of dynamic libraries to load before the ones specified in the
  program.

 To be widely injected the environment variable needs to be placed within the "launchctl setenv" command that sets the variable in the global scope, this makes any process load the library when launched.

### loading: ###

 The command is executed by "LaunchDeamons" (similar to services) from the "environment.plist" file, and that what makes the environment variable being set after reboots and resprings (load method).
 
###  hooking: ###

The hooking method is accomplished by rebinding functions simbols:

   * At first we get the original functions simbols (dlsym()) so we will be able to call the original ones after adding our code to the functions. 
   * Our functions needs to be called the same as the original functions.
   * When the environment variable DYLD_FORCE_FLAT_NAMESPACE is set to "1" (can be set from the environment.plist too) it forces all images in the program to be linked as flat-namespace images and ignore any two-level
    namespace bindings and from now and then when the relevant functions are called it'll be our functions and not the original ones.

*These methods can be used for a variety of uses and not only for certificate validation cancellation.*

## How to make it work? ##

The library itself needs to be placed in /usr/lib/ path so it'll be considered an suitable image to be loaded.
When the plist file is located in the LaunchDeamonds path the "launchctl load -w <PLIST PATH>" command needs to be executed and a reboot is needed.