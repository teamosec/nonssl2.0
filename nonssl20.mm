/*=========================
|
|     FOR DEBUGING ONLY		
|
==========================*/

#include <dlfcn.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>

#import <Foundation/Foundation.h>
#import <Foundation/NSObjCRuntime.h>


typedef OSStatus(*func_SSLSetSessionOption)(SSLContextRef context, SSLSessionOption option, BOOL value);
typedef OSStatus(*func_SSLHandshake)(SSLContextRef context);
typedef SSLContextRef(*func_SSLCreateContext)(CFAllocatorRef alloc, SSLProtocolSide protocolSide, SSLConnectionType 
connectionType);

static func_SSLSetSessionOption orig_SSLSetSessionOption = NULL;
static func_SSLHandshake orig_SSLHandshake = NULL;
static func_SSLCreateContext orig_SSLCreateContext = NULL;

extern void getOriginalSymbols();

OSStatus SSLSetSessionOption(SSLContextRef, SSLSessionOption, BOOL);
OSStatus SSLHandshake(SSLContextRef);
SSLContextRef SSLCreateContext(CFAllocatorRef, SSLProtocolSide, SSLConnectionType);


//Declaring constructor that will call the __attach function when library is loaded
int __attach(void) __attribute__((constructor));


int __attach(void)
{	
	@autoreleasepool { 
		NSLog(@"Library Loaded");
		sleep(1);

		NSLog(@"Getting SecureTransport simbols");
		getOriginalSymbols();
		NSLog(@"SetSession OriginalSymbol: %p", orig_SSLSetSessionOption);
		NSLog(@"Handshake OriginalSymbol: %p", orig_SSLHandshake);
		NSLog(@"CreateContext OriginalSymbol: %p", orig_SSLCreateContext);

		return 0;
	}
}


//Retrieves the original SecureTeansport SSL functions simbols.
void getOriginalSymbols() {
	orig_SSLHandshake = (func_SSLHandshake)dlsym(RTLD_NEXT, "SSLHandshake");
	orig_SSLSetSessionOption = (func_SSLSetSessionOption)dlsym(RTLD_NEXT, "SSLSetSessionOption");
	orig_SSLCreateContext = (func_SSLCreateContext)dlsym(RTLD_NEXT, "SSLCreateContext");
}


//Set any ssl session with kSSLSessionOptionBreakOnServerAuth means every unauthorized certifecate 
//will be tested again by the application itself.
OSStatus SSLSetSessionOption(SSLContextRef context, SSLSessionOption option, BOOL value) {
	NSLog(@"OPTION");

	if (!orig_SSLSetSessionOption) {
        	orig_SSLSetSessionOption = (OSStatus (*)(SSLContextRef, SSLSessionOption, BOOL)) dlsym(RTLD_NEXT, 
"SSLSetSessionOption");
	}

        return (*orig_SSLSetSessionOption)(context, kSSLSessionOptionBreakOnServerAuth, value);
}


//When handshaking in case of using unauthorized certifecate handshake function will return 
// errSSLServerAuthCompleted value when the session option is set to kSSLSessionOptionBreakOnServerAuth
//and then if the application takes responsebilty on the certifecate it'll call the handshake again.
OSStatus SSLHandshake(SSLContextRef context) {
	NSLog(@"HANDSHAKE");
	
	if (!orig_SSLHandshake) {
        	orig_SSLHandshake = (OSStatus (*)(SSLContextRef)) dlsym(RTLD_NEXT, "SSLHandshake");
	}

        OSStatus SSLHandshakeReturnValue = (*orig_SSLHandshake)(context);

        if (SSLHandshakeReturnValue == errSSLServerAuthCompleted) {
                return (*orig_SSLHandshake)(context);
	}
        else {
                return SSLHandshakeReturnValue;
	}
}


//When creating a session we will set the session option to accept application validation.
SSLContextRef SSLCreateContext(CFAllocatorRef alloc, SSLProtocolSide protocolSide, SSLConnectionType 
connectionType) {
	NSLog(@"CONTEXT");

        if (!orig_SSLCreateContext) {
                orig_SSLCreateContext = (SSLContextRef (*)(CFAllocatorRef, SSLProtocolSide, SSLConnectionType)) dlsym(RTLD_NEXT, "SSLCreateContext");
	}

        SSLContextRef sslContext = (*orig_SSLCreateContext)(alloc, protocolSide, connectionType);
    
        (*orig_SSLSetSessionOption)(sslContext, kSSLSessionOptionBreakOnServerAuth, true);

        return sslContext;
}

